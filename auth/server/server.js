const https = require('https');
const http  = require('http');
const fs    = require('fs');
const express = require('express');
const bodyParser = require('body-parser');


const OK = 200;
const CREATED = 201;
const NO_CONTENT = 204;
const SEE_OTHER = 303;
const BAD_REQUEST = 400;
const UNAUTHORIZED = 401;
const NOT_FOUND = 404;
const SERVER_ERROR = 500;

const notfound = {status: "ERROR_NOT_FOUND", info: "user ID not found" };
const success = {status: "OK", authtoken:"authToken" };  // upon login success.
const created = {status:"CREATED",authtoken:"authToken"};
const exists = {status:"EXISTS",info:"user already exists"};
const notFound = {status: "ERROR_NOT_FOUND", info: "user not found"};
const unauthorized = {status: "ERROR_UNAUTHORIZED",info: "/users/ID/auth requires a valid 'pw' password query parameter"};
const authError =  { status: "ERROR_UNAUTHORIZED", info: "/users/ID requires a bearer authorization header" };


function serve(options, model) {
    const app = express();
    app.locals.model = model;
    const authTimeout = options.authTimeout;
    app.locals.authTimeout = options.authTimeout;

    app.locals.port = options.port;
    const KEY_PATH = options.sslDir;
    const CERT_PATH = options.sslDir;
    app.locals.userslist = [];
    setupRoutes(app);

    /* HTTP Server
    http.createServer(app).listen(app.locals.port, function() {
        console.log(`HTTP Server Listening on port ${app.locals.port}`);
    });
    */

    https.createServer({
        key: fs.readFileSync(KEY_PATH+"/key.pem"), // KEY_PATH
        cert: fs.readFileSync(CERT_PATH+"/cert.pem")},app).listen(app.locals.port); //CERT_PATH
}

function setupRoutes(app)
{
    app.get('/users/:ID',getUser(app));
    app.use('/users/:ID', bodyParser.json());
    app.put('/users/:ID',registerationWS(app));
    app.put('/users/:ID/auth',loginWS(app));
}

function loginWS(app){

    return function(request,response) {
        const id = request.params.ID;
        const pwd   = request.body.pw;
        if(typeof pwd ==='undefined')
        {
            unauthorized.info = "user/"+id+"/auth requires a valid 'pw' password query parameter";
            response.status(UNAUTHORIZED).send(unauthorized);
            return;
        }

        request.app.locals.model.users.hashPassword(pwd, function(pwd){

            if (typeof id === 'undefined') {
                response.sendStatus(BAD_REQUEST);
            }
            else {
                request.app.locals.model.users.findUser(id).
                then(function(user) {
                    if(user){
                        if(user.pwd === pwd)   //compares hashes.
                        {
                            //  console.log("LoginWS:findUser: "+JSON.stringify(user));
                            let authtoken = String(Math.random().toString(36).substr(2, 7));
                            let timestamp = Math.floor(Date.now() / 1000);
                            let localfind = false;

                            for( var i=0;i<request.app.locals.userslist.length;i++)
                            {
                                if(user._id == request.app.locals.userslist[i]._id)
                                {
                                    localfind = true;
                                    request.app.locals.userslist[i].timestamps.push(timestamp);
                                    request.app.locals.userslist[i].authtokens.push(authtoken);
                                    // console.log(request.app.locals.userslist[i]  );          // print user's complete data
                                }
                            }
                            if(!localfind)
                            {
                                var uobj = { _id:user._id, timestamps:[], authtokens:[] } ;
                                uobj.timestamps.push(timestamp);
                                uobj.authtokens.push(authtoken);
                                request.app.locals.userslist.push(uobj);
                            }
                            success.authtoken = authtoken;
                            response.status(OK).send(JSON.stringify(success));
                        }
                        else
                        {    //invalid password
                            unauthorized.info = "user/"+id+"/auth requires a valid 'pw' password query parameter";
                            response.status(UNAUTHORIZED).send(JSON.stringify(unauthorized));
                        }
                    }
                    else
                    {
                        var string ="user ID not found";
                        notfound.info =string.replace("ID",id);
                        response.status(NOT_FOUND).send(notfound);
                    }});
            }
        });
    }

}


function getUser(app){

    return function(request,response) {

        const id = request.params.ID;

        if (typeof id === 'undefined') {
            response.sendStatus(BAD_REQUEST);
        }
        else {
            request.app.locals.model.users.findUser(id).
            then(function(user) {
                if(user)
                {
                    //console.log(" : ",user);
                    if (!request.headers.authorization)
                    {
                        authError.info = "user/"+id+"/auth requires a bearer authorization header";
                        response.status(UNAUTHORIZED).send(authError);
                        return;
                    }
                    let reqhead = request.headers.authorization.split(' ');

                    if(typeof reqhead[0]!== 'string' || reqhead[0].toLowerCase() !== "bearer" || typeof reqhead[1] ==='undefined')
                    {
                        authError.info = "user/"+id+"/auth requires a valid bearer authorization header";
                        response.status(UNAUTHORIZED).send(authError);
                        return;
                    }
                    let authtoken = reqhead[1];
                    let currentTS = Math.floor(Date.now() / 1000);
                    var isValid;

                    authenticateCredentials(request,authtoken,currentTS,user, function(isValid,user) {  //helper fn.
                        if(isValid){
                            //console.log("IsValid--> valid=true ");
                            response.send(user.DATA);
                        }
                        else
                        {
                            // console.log("isValid--> valid = false");
                            authError.info = "user/"+id+"/auth requires a bearer authorization header";
                            response.status(UNAUTHORIZED).send(authError);
                        }});
                }
                else{
                    notFound.info ="user "+id +" not found";
                    response.status(NOT_FOUND).send(notFound);
                }
            });
        }
    }}



function authenticateCredentials(request,authtoken,currentTS,user,callback){
    let valid;
    let processed = false;
    // let found = false;

    for( let i=0;i<request.app.locals.userslist.length;i++)
    {

        if(user._id == request.app.locals.userslist[i]._id)
        {
            for(var j=0;j<request.app.locals.userslist[i].authtokens.length;j++)
            {
                if( authtoken === request.app.locals.userslist[i].authtokens[j] )
                {
                    //console.log("Token Created time: ",request.app.locals.userslist[i].timestamps[j]);
                    //console.log("Token valid upto: ",request.app.locals.userslist[i].timestamps[j]+ request.app.locals.authTimeout);
                    //console.log("Current Time: ",currentTS);

                    if(currentTS < request.app.locals.userslist[i].timestamps[j] + request.app.locals.authTimeout )
                    {
                        valid =  true;
                    }
                    else
                    {  valid = false;
                    }
                    processed = true;
                    callback(valid,user);
                }
            }
        }
    }
    if(!processed){
        callback(false,user);
    }
}



function registerationWS(app)
{
    return function(request,response){

        const id = request.params.ID;
        const pwd = request.query.pw;

        if(!pwd)
        {
            unauthorized.info = "user/"+id+"/auth requires a valid 'pw' password query parameter";
            response.status(UNAUTHORIZED).send(unauthorized);
            return;
        }
        const content = request.body;

        if (typeof id === 'undefined') {
            response.sendStatus(BAD_REQUEST);
        }
        else {
            request.app.locals.model.users.findUser(id).
            then(function(user) {
                if(user){
                    response.append('Location',`${request.protocol}://${request.hostname}:${request.app.locals.port}` + "/users/"+id);
                    exists.info = "user "+id+" already exists";
                    response.status(SEE_OTHER).send(exists);
                }
                else{
                    request.app.locals.model.users.hashPassword(pwd, function(pwd){

                        const authtoken = Math.random().toString(36).substr(2, 7);
                        const timestamp = Math.floor(Date.now() / 1000);

                        request.app.locals.model.users.createUser(id,pwd,content).
                        then(function(id1) {

                            var uobj = { _id:id, timestamps:[], authtokens:[] } ;
                            uobj.timestamps.push(timestamp);
                            uobj.authtokens.push(authtoken);
                            request.app.locals.userslist.push(uobj);
                            response.append('Location',`${request.protocol}://${request.hostname}:${request.app.locals.port}` + "/users/"+id);
                            created.authtoken = authtoken;
                            response.status(CREATED).send(created);
                        }).catch((err) => {
                            console.error(err);
                        response.sendStatus(SERVER_ERROR); });
                    });
                }
            }).catch((err) => {
                console.error(err);
            response.sendStatus(SERVER_ERROR);
        });
        }
    }
}

function requestUrl(req) {
    const port = req.app.locals.port;
    return `${req.protocol}://${req.hostname}:${port}${req.originalUrl}`;
}

module.exports = {
    serve: serve
}
