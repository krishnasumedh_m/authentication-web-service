const assert = require('assert');
var crypto = require('crypto');

const USERS = 'usr';
function Users(db)
{
    this.db = db;
    this.users = db.collection(USERS);
}

function initUsers(db)
{
    return new Promise(function(resolve, reject) {
        const collection = db.collection(USERS);
        resolve(db);
    });
}

Users.prototype.findUser = function(id){

    const searchSpec = { _id: id };
    return this.users.find(searchSpec).toArray().
    then(function(users) {
        return new Promise(function(resolve, reject) {
            if (users.length === 1) {
                resolve(users[0]);
            }
            else if (users.length == 0) {
                resolve(null);
            }
            else {
                reject(new Error(`cannot find user ${id}`));
            }
        });
    });
};

Users.prototype.createUser = function(id,pwd,content){

    const uobj = { _id: id, pwd: pwd, DATA: content };
    return this.users.insert(uobj).
    then(function(results) {
        return new Promise((resolve) => resolve(results));
    });
};

Users.prototype.hashPassword = function(password,callback)
{
    var hash = crypto.createHash('sha256').update(password).digest('base64');
    callback(hash);

}

module.exports = {
    Users: Users,
    inUsers: initUsers
};
