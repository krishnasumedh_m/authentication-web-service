#!/usr/bin/env nodejs

const assert = require('assert');
const mongo = require('mongodb').MongoClient;
const process = require('process');
const users = require('./model/users');
const model = require('./model/model');
const server = require('./server/server');
const option   = require('./options');
const DB_URL = 'mongodb://localhost:27017/users';
const ret = option.options;
console.log(ret);

mongo.connect(DB_URL).then((db) => users.inUsers(db)).
then(function(db) {
    const model1 = new model.Model(db);
    server.serve(ret, model1);
    //db.close();
}).
catch((e) => console.error(e));
