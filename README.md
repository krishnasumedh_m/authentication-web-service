Some key implementation considerations -

-> This is a Web service built using using Nodejs and Expressjs.

-> Developed a RESTful API for account creation,registration and authentication of users and is ready to be consumed by any client such as any web-app/    
   website which needs to use the authentication functionality instead of implementing its own logic and avoid reinvent the wheel.
   
-> The user's password is hashed and is only then handed over to the mongo database for storage. Used sha-256 algorithm for hashing purpose.  
   (  http://blog.moertel.com/posts/2006-12-15-never-store-passwords-in-a-database.html  )
  
-> Once a user registers to access a service, there is no need to login at that time again. 
 
-> Generates Authentication tokens upon successful logins and associates them with timestamps. The tokens expire after the time period as per the 
   timestamp hence causing users to access resources requiring authentication only within a certain time .   
  

